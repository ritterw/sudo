package com.example.sudo;

import android.R.integer;
import android.R.string;
import android.support.v7.app.ActionBarActivity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends ActionBarActivity implements OnClickListener {

	private View mycontinue, mynew, myabout, myexit, mysetting;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mycontinue = (Button) super.findViewById(R.id.continue_button);
		mynew = (Button) super.findViewById(R.id.new_button);
		myabout = (Button) super.findViewById(R.id.about_button);
		mysetting = (Button) super.findViewById(R.id.setting_button);
		myexit = (Button) super.findViewById(R.id.exit_button);

		mycontinue.setOnClickListener(this);
		mynew.setOnClickListener(this);
		myabout.setOnClickListener(this);
		mysetting.setOnClickListener(this);
		myexit.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.continue_button:
			

			break;

		case R.id.new_button:
			openNewGameDialog();

			break;

		case R.id.about_button:
			Intent i = new Intent(this, about.class);
			startActivity(i);

			break;

		case R.id.setting_button:

			break;

		case R.id.exit_button:
			finish();
			break;

		default:
			finish();
			break;
		}

	}

	private static final String TAG = "SuDo";

	private void openNewGameDialog() {
		// TODO Auto-generated method stub
		new AlertDialog.Builder(this)
				.setTitle(R.string.newgame_title)
				.setItems(R.array.difficulty,
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								startGame(which);
							}

						}).show();
	}

	private void startGame(int i) {
		// TODO Auto-generated method stub
		Log.d(TAG, "clicked on " + i);
		// start game here...
		Intent intent = new Intent(this, game.class);
		intent.putExtra(Game.KEY_DIFFICULTY, i);
		startActivity(intent);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent i = new Intent(this, perfs.class);
			startActivity(i);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
