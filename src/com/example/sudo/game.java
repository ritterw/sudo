package com.example.sudo;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import android.view.Gravity;
import android.app.Dialog;

public class game extends Activity{
	private static final String TAG = "SuDo";
	
	public static final String KEY_DIFFCULY = 
			"org.example.sudo.difficulty";
	public static final int DIFFICULY_EASY = 0;
	public static final int DIFFICULY_MEDIUM = 1;
	public static final int DIFFICULY_HARD = 2;
	
	private int puzzle[] = new int[9 * 9];
	
	private PuzzleView puzzleView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Log.d(TAG, "OnCreate");
		
		int diff = getIntent().getIntExtra(KEY_DIFFCULY, DIFFICULY_EASY);
		puzzleView = getPuzzle(diff);
		
		calculateUsedTiles();
		
		setContentView(puzzleView);
		puzzleView.requestFocus();
		
	}
	

}
